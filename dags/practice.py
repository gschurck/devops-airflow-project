from datetime import timedelta

import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator

args = {
    'id': 'practice',
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'schedule_interval': timedelta(days=1),
}
dag = DAG(
    'practice',
    default_args=args,
    description='A simple tutorial DAG',
    schedule_interval=timedelta(days=1),
)

t1 = BashOperator(
    task_id='t1',
    bash_command='echo "{{task_instance_key_str}}"',
    dag=dag,
)

t2 = BashOperator(
    task_id='t2',
    bash_command='echo "{{task_instance_key_str}}"',
    dag=dag,
)


t3 = BashOperator(
    task_id='t3',
    bash_command='echo "{{task_instance_key_str}}"',
    dag=dag,
)

t4 = BashOperator(
    task_id='t4',
    bash_command='echo "{{task_instance_key_str}}"',
    dag=dag,
)

t_final = BashOperator(
    task_id='t_final',
    bash_command='echo "CIAO"',
    dag=dag,
)

t1 >> t_final