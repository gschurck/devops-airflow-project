import requests, datetime, pymongo, os
from airflow.operators.python_operator import PythonOperator
from dotenv import load_dotenv

# -*- coding: utf-8 -*-
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

"""
### Tutorial Documentation
Documentation that goes along with the Airflow tutorial located
[here](https://airflow.incubator.apache.org/tutorial.html)
"""
from datetime import timedelta

import airflow
from airflow import DAG

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'project',
    default_args=default_args,
    description='A simple tutorial DAG',
    schedule_interval=timedelta(seconds=30),
)


def test():
    print("test")
    return "test"


def insert_data():
    load_dotenv()
    API_KEY = os.getenv('API_KEY')
    print(API_KEY)
    data = {}
    r = requests.get('https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=' + API_KEY)
    res = r.json()
    data["profile"] = res[0]
    r = requests.get('https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=' + API_KEY)
    res = r.json()
    data['rating'] = res[0]['rating']
    data['timestamp'] = datetime.datetime.now()
    db = pymongo.MongoClient().airflow
    db.project.insert_one(data)


t1 = PythonOperator(
    task_id='print_date',
    python_callable=test,
    dag=dag,
)

# t1 >> [t2, t3]
#
# load_dotenv()
# API_KEY = os.getenv('API_KEY')
# print(API_KEY)
# data = {}
# r = requests.get('https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=' + API_KEY)
# res = r.json()
# data["profile"] = res[0]
# r = requests.get('https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=' + API_KEY)
# res = r.json()
# data['rating'] = res[0]['rating']
# data['timestamp'] = datetime.datetime.now()
# MONGO_URI = "mongodb://mongadmin:secret@localhost:27017"
# db = pymongo.MongoClient().airflow
# db.project.insert_one(data)
