import requests, datetime, pymongo, os
from dotenv import load_dotenv

load_dotenv()
API_KEY = os.getenv('API_KEY')
print(API_KEY)
data = {}
r = requests.get('https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=' + API_KEY)
res = r.json()
data["profile"] = res[0]
r = requests.get('https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=' + API_KEY)
res = r.json()
data['rating'] = res[0]['rating']
data['timestamp'] = datetime.datetime.now()
MONGO_URI = "mongodb://mongadmin:secret@localhost:27017"
db = pymongo.MongoClient().airflow
db.project.insert_one(data)
